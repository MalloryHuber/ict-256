$(function () {

    $.validator.addMethod("PWCHECK",function (value, element) {
        if( /^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&^'/#]){1,}).{8,}$/.test(value)){
            return true;
        }else{
            return false;
        };
    });

    $('#inscription_form').validate(
        {
            rules:{
                nom_per: {
                    required:true,
                    minlength:2
                },
                prenom_per:{
                    required:true,
                    minlength:2
                },
                mail_per:{
                    required:true,
                    email:true
                },
                password_per:{
                    required:true,
                    PWCHECK:true
                },
                confirm_password_per:{
                    equalTo:"#password_per"
                }
            },
            messages:{
                nom_per:{
                    required:"Veuillez saisir votre nom",
                    minlength:"Votre nom doit être composé de 2 caractères au minimum"
                },
                prenom_per:{
                    required:"Veuillez saisir votre prénom",
                    minlength:"Votre prénom doit être composé de 2 caractères au minimum"
                },
                mail_per:{
                    required:"Veuillez saisir votre email",
                    email:"Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                },
                password_per: {
                    required:"Veuillez saisir votre mot de passe",
                    minlength:"Votre mot de passe est trop court",
                    PWCHECK:"Le mot de passe doit comporter au minimum 8 caractères, dont une minuscule, une majuscule, un chiffre et un caractère spécial"
                },
                confirm_password_per: {
                    equalTo: "Les mots de passe ne sont pas identiques",
                    required:"Veuillez saisir une deuxième fois votre mot de passe"
                }
            }
        }
    )
});